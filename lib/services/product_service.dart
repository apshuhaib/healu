import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:healu_app/application/features/Products/model/product_model.dart';

class ProductService {
  final CollectionReference _productCollection =
      FirebaseFirestore.instance.collection('products');

  //create product

  Future<ProductModel?> createProduct(ProductModel product) async {
    try {
      final productMap = ProductModel().toMap();
      await _productCollection.doc(product.id).set(productMap);
      return product;
    } on FirebaseException catch (e) {
      print(e.toString());
    }
  }

  //get All products
  Stream<List<ProductModel>> getAllProducts() {
    try {
      return _productCollection.snapshots().map((QuerySnapshot snapshot) {
        return snapshot.docs.map((DocumentSnapshot doc) {
          return ProductModel.fromJson(doc);
        }).toList();
      });
    } on FirebaseException catch (e) {
      print(e.toString());
      throw e;
    }
  }
  //update products

  Future<void> updateProduct(ProductModel product) async {
    try {
      final productMap = product.toMap();
      await _productCollection.doc(product.id).update(productMap);
    } on FirebaseException catch (e) {
      print(e.toString());
    }
  }

  Future<void> deleteProduct(String? id) async {
    try {
      await _productCollection.doc(id).delete();
    } on FirebaseException catch (e) {
      print(e.toString());
    }
  }

  //delete products
}
