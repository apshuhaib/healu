import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:healu_app/application/features/Products/add_products_page.dart';
import 'package:healu_app/application/features/Products/model/product_model.dart';
import 'package:healu_app/services/product_service.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Products',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/addProduct');
                    },
                    icon: Icon(
                      Icons.add,
                      color: Colors.white,
                    ))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Expanded(
                child: StreamBuilder(
                    stream: FirebaseFirestore.instance
                        .collection('products')
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.hasError) {
                        return Center(
                          child: Text('Occured some error'),
                        );
                      }

                      if (snapshot.hasData && snapshot.data!.docs.length == 0) {
                        return Center(
                          child: Text('No data Found'),
                        );
                      }
                      if (snapshot.hasData && snapshot.data!.docs.length != 0) {
                        return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              final _product = ProductModel.fromJson(
                                  snapshot.data!.docs[index]);
                              return Card(
                                child: ListTile(
                                  title: Text('${_product.productName}'),
                                  subtitle: Text('${_product.details}'),
                                  trailing: Container(
                                    width: 100,
                                    child: Row(
                                      children: [
                                        IconButton(
                                            onPressed: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                      builder: (context) {
                                                return AddProductPage(
                                                  product: _product,
                                                );
                                              }));
                                            },
                                            icon: Icon(
                                              Icons.edit,
                                              color: Colors.teal,
                                            )),
                                        IconButton(
                                            onPressed: () {
                                              ProductService()
                                                  .deleteProduct(_product.id);
                                            },
                                            icon: Icon(
                                              Icons.delete,
                                              color: Colors.red,
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            });
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }))
          ],
        ));
  }
}
