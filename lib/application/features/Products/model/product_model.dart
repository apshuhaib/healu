import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

class ProductModel {
  String? id;
  String? productName;
  String? details;
  int? status;
  DateTime? createdAt;

  ProductModel(
      {this.id, this.productName, this.details, this.status, this.createdAt});

  factory ProductModel.fromJson(DocumentSnapshot json) {
    Timestamp? timestamp = json['createdAt'];
    return ProductModel(
      id: json['id'],
      productName: json['productName'],
      details: json['details'],
      status: json['status'],
      createdAt: timestamp?.toDate(),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productName': productName,
      'details': details,
      'status': status,
      'createdAt': createdAt
    };
  }
}
