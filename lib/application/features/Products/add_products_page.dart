import 'package:flutter/material.dart';
import 'package:healu_app/application/features/Products/model/product_model.dart';
import 'package:healu_app/services/product_service.dart';
import 'package:uuid/uuid.dart';

class AddProductPage extends StatefulWidget {
  final ProductModel? product;
  const AddProductPage({super.key, this.product});

  @override
  State<AddProductPage> createState() => _AddProductPageState();
}

class _AddProductPageState extends State<AddProductPage> {
  TextEditingController _productTitleController = TextEditingController();
  TextEditingController _productDescriptionController = TextEditingController();

  @override
  void dispose() {
    _productTitleController.dispose();
    _productDescriptionController.dispose();
    super.dispose();
  }

  final _productKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xfff263147),
          iconTheme: const IconThemeData(color: Colors.white),
        ),
        body: Container(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _productKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Add Products',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _productTitleController,
                  style: const TextStyle(color: Colors.white, fontSize: 18),
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.white),
                    border: OutlineInputBorder(),
                    hintText: 'Add product',
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: _productDescriptionController,
                  style: const TextStyle(color: Colors.white, fontSize: 18),
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.white),
                    border: OutlineInputBorder(),
                    hintText: 'Details',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: InkWell(
                    onTap: () {
                      if (_productKey.currentState!.validate()) {
                        _addProduct();
                      }
                    },
                    child: Container(
                      color: Colors.teal,
                      height: 48,
                      width: 250,
                      child: const Center(
                          child: Text(
                        'Add',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  _addProduct() async {
    var id = Uuid().v1();
    ProductModel _productModel = ProductModel(
        productName: _productTitleController.text,
        details: _productDescriptionController.text,
        id: id,
        status: 1,
        createdAt: DateTime.now());

    ProductService _productService = ProductService();
    _productService.createProduct(_productModel);
    final product = await _productService.createProduct(_productModel);
    if (product != null) {
      print(product.details);
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Product added')));
    }
  }
}
