part of 'auth_bloc.dart';

@immutable
sealed class AuthEvent {}

//check login
class checkLoginStatusEvent extends AuthEvent {}

//login event
class LoginEvent extends AuthEvent {
  final String email;
  final String password;

  LoginEvent({required this.email, required this.password});
}

//signUp event
class SignUpEvent extends AuthEvent {
  final UserModel user;

  SignUpEvent({required this.user});
}

//logout event
class LogOutEvent extends AuthEvent {}
