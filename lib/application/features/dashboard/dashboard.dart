import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:healu_app/application/features/Products/model/product_model.dart';
import 'package:healu_app/services/product_service.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({super.key});

  @override
  State<DashBoardPage> createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  ProductService _productService = ProductService();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Dashboard',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'latest Products',
              style: TextStyle(color: Colors.white),
            ),
            const SizedBox(
              height: 10,
            ),
            Divider(),
            Expanded(
                child: StreamBuilder<List<ProductModel>>(
                    stream: _productService.getAllProducts(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.hasError) {
                        return Center(
                          child: Text('Occured some error'),
                        );
                      }

                      if (snapshot.hasData && snapshot.data!.length == 0) {
                        return Center(
                          child: Text('No data Found'),
                        );
                      }
                      if (snapshot.hasData && snapshot.data!.length != 0) {
                        List<ProductModel> products = snapshot.data ?? [];
                        return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              final _product = products[index];
                              print(_product);
                              return Card(
                                color: const Color(0xfff263147),
                                child: ListTile(
                                  title: Text(
                                    '${_product.productName}',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  subtitle: Text(
                                    '${_product.details}',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              );
                            });
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }))
          ],
        ));
  }
}
