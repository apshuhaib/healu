import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:healu_app/application/features/Products/add_products_page.dart';
import 'package:healu_app/application/features/auth/views/login_page.dart';
import 'package:healu_app/application/features/auth/views/user_resgitration_page.dart';
import 'package:healu_app/application/features/home/views/home_page.dart';
import 'package:healu_app/application/features/splash/splash_page.dart';
import 'package:healu_app/firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'healu',
      theme: ThemeData(
        textTheme: const TextTheme(
            displayLarge: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 26),
            displaySmall: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16)),
        scaffoldBackgroundColor: const Color(0xfff263147),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const SplashPageWrapper(),
        '/home': (context) => const HomePageWrapper(),
        '/login': (context) => const LoginPageWrapper(),
        '/register': (context) => const RegisterUserPageWrapper(),
        '/addProduct': (context) => AddProductPage()
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(child: Text('home')),
    );
  }
}
